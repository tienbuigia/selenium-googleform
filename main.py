from selenium import webdriver
from selenium.webdriver.common.by import By
import argparse
import random
from faker import Faker
import time

# argument parsing
arg_parser = argparse.ArgumentParser(
    description="This script is used to auto fill GForm.")
arg_parser.add_argument('--vietnam',
                        action='store_true',
                        help='Specifiy if Vietnam is True')
arg_parser.add_argument('--iterations',
                        type=int,
                        default=3,
                        help='Number of iterations')
arg_parser.add_argument('--nosubmit',
                        action='store_true',
                        help='Not submit, for testing')
args = arg_parser.parse_args()

driver = webdriver.Firefox()
fake = Faker()
names = [
    "tien", "hung", "cuong", "thang", "hoang", "ngoc", "nhung", "thuy", "nga",
    "vui", "tuoi", "nam", "loc", "hieu", "linh", "ha"
]
lastnames = [
    "bui", "nguyen", "dinh", "tran", "le", "pham", "hoang", "huynh", "phan",
    "vu", "dang", "do", "ho", "ngo", "duong", "ly"
]
domains = [
    "gmail.com", "protonmail.com", "pm.me", "cock.ly", "proton.me",
    "yahoo.com", "gmail.com", "gmail.com", "yahoo.com.vn"
]
professions = [
    "Kinh doanh tự do", "Lập trình viên", "Kinh doanh nhà hàng",
    "Kinh doanh bất động sản", "Kinh doanh", "Thiết kế phần mềm"
]

url = "https://forms.gle/bfcK7ZA6773YMEnh8"


def open_fillform(vietnam=False):
    driver.get(url)

    if vietnam:
        random_email = random.choice(names) + '_' + random.choice(
            lastnames) + '@' + random.choice(domains)
        random_job = random.choice(professions)
        national_button = driver.find_element(By.CSS_SELECTOR, '#i28')
    else:
        random_email = fake.name().lower().replace(
            " ", "_") + '@' + random.choice(domains)
        random_job = fake.job()
        national_button = driver.find_element(By.CSS_SELECTOR, '#i31')

    email_input = driver.find_element(By.CSS_SELECTOR,
                                      'textarea[aria-labelledby="i3"]')
    job_input = driver.find_element(By.CSS_SELECTOR,
                                    'input[aria-labelledby="i20"]')

    # email
    email_input.send_keys(random_email)

    # gender
    gender_ids = ["i11", "i14"]
    select_random_1(gender_ids)

    # job
    job_input.send_keys(random_job)

    # national
    national_button.click()

    # age
    age_ids = ["i38", "i41", "i44", "i47"]
    select_random_1(age_ids)

    # sources
    source_ids = ["i54", "i57", "i60"]
    select_random_1(source_ids)

    # services
    service_ids = ["i71", "i74", "i77", "i80", "i83"]
    selected_sv_ids = random.sample(service_ids, 3)
    select_all(selected_sv_ids)

    # rating
    entries = [
        "entry.838494581_sentinel", "entry.515266814_sentinel",
        "entry.2145557953_sentinel", "entry.1820572056_sentinel",
        "entry.544869065_sentinel"
    ]
    for entry in entries:
        rate_random_three_four(entry)

    # services to improve
    improve_ids = ["i98", "i101", "i104"]
    select_all(improve_ids)


def select_random_1(ids):
    random_id = random.choice(ids)
    button = driver.find_element(By.CSS_SELECTOR, f'#{random_id}')
    button.click()


def select_all(ids):
    for id in ids:
        button = driver.find_element(By.CSS_SELECTOR, f'#{id}')
        button.click()


def rate_random_three_four(name):
    buttons = driver.find_elements(
        'xpath', f'//*[@name="{name}"]/preceding-sibling::*')[3:5]
    random.choice(buttons).click()


def submit():
    driver.find_element(
        'xpath',
        '//*[@id="mG61Hd"]/div[2]/div/div[3]/div[1]/div/div/span/span').click(
        )


if __name__ == "__main__":

    if args.nosubmit:
        open_fillform(args.vietnam)
    else:
        for _ in range(args.iterations):
            open_fillform(args.vietnam)
            submit()
            time.sleep(5)

        driver.quit()
